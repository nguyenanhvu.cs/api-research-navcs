var glob = require("glob");
var path = require("path");
var nodeExternals = require("webpack-node-externals");

module.exports = {
  entry: glob.sync("./+(functions|migrations)/**.js").reduce((entries, path) => {
    entries[path.substring(0, path.length - 3)] = path;
    return entries;
  }, {}),
  target: "node",
  externals: [nodeExternals()],
  module: {
    rules: [{
      test: /\.js$/,
      loader: "babel-loader",
      include: __dirname,
      exclude: /node_modules/,
    }]
  },
  output: {
    libraryTarget: "commonjs",
    path: path.join(__dirname, ".webpack"),
    filename: "[name].js"
  },
};
